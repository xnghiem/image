##Observer Design Pattern##
### Gang of Four (GoF) classification ###
**The Observer Design Pattern**, also known as the Publish-Subscribe Pattern, is a behavioral design pattern that allows for communication between different observers to the object, known as the subject. It is one of the 23 well-known  "Gang of Four" design patterns that describes how to design flexible and reusable object-oriented software. It helps to create objects that are easier to implement, change, test, and reuse. Below is an example UML diagram.

#### Example UML diagram ####

![alt text](https://static.dzone.com/dz1/dz-files/observer_pattern.PNG "Logo Title Text 1")

The Observer Design Pattern provides communications between the subject class and the observer classes through the update() method. 

Typical implementations of the Observer pattern as shown in the diagram includes the subject interface, consisting primary of 4 methods attach(), detach(), setState(), and notify(). This provides an interface to attach and detach observer objects. The observer interface provides an update method that is called by the subject to notify it of the subject's changing states. The concreteSubject class stores the state of interest of the concreteObserver objects. It is responsible for sending notifications to its observers. The concreteObserver class maintains references to the concreteSubject class and maintains the observer state. The concreteObserver class is responsible for updating the operation.

### GoF Intent ###
#### What does the design pattern do? ####
**The Observer Design Pattern** provides a one-to-many dependency relationship between different objects, thus allowing for automatic updates and communications between different objects. In this case one object, known as the subject or publisher, maintains constant communications between multiple dependencies, called observers or subscribers. Each subject can publish changes to its state, which automatically notifies the dependencies of state changes for them to act accordingly. Subjects are allowed any number of dependencies in which to notify and any number of observers can subscribe to the subject in which it had received the notifications. 

#### What is its rationale or intent? ####

The main intent is to provide loose coupling between an observable subject and one or more observers. It is used for cases where objects requires state changes of other objects and wants to perform specific actions based on the state changes. A common example is the click button in an app. When the button is clicked, objects connected to the event can subscribe to it and perform some actions. 

The main purpose of the observer pattern is to have a subject that automatically notifies its connected observers of state changes by calling one of their methods

#### What particular design issue or problem does it address? ####
The problems that it addresses/solves are:
*A one-to-many dependency between objects can be defined without making the objects tightly coupled

*It can help to ensure that when one object changes state an open-ended number of dependent objects are updated automatically.

*It allows one object to notify an open-ended number of other objects.

Directly defining a one to many dependency between objects so that one object can update the state of other dependent objects creates tight coupling, which means it creates inflexibility. Tightly coupled objects are hard to implement, change, test, and reuse with issues of dependencies. The observer patterns solves this issue by defining objects that are loosely coupled. 

The observer pattern provides communication between the subject and observer objects so that when the subject changes states, all registered observers are notified of the change and can update their states automatically. The subject is responsible for maintaining a list of observers to notify and the observers' responsibilities are to register themselves on a subject in order to be notified of the subject's state changes and to change their states once notified. This creates loose coupling which makes the subjects and observers independent, making the extensions, removal or additions, of methods within the observer and subject classes independent. 

The Observer design pattern helps to address the following issues:
* Adding a new dependent object
* Replacing an existing dependent object with a new object
* Removing an existing dependent object

The Observer pattern allows developers to modify the subjects and observers independently. Subjects can be reused without reusing the observers attached to it and vice versa. Observers can be added without modifying the subject or other observers. Thus, the pattern adheres to the open for extensions, but closed for modification principle.

#### GoF Intent ####
According to GoF, the observer pattern intent is:

"Define a one-to-many dependency between
objects so that when an object changes its
state, all its dependents are notified and
updated automatically."


#### When should you use the design pattern? ####
The design pattern can be used in the following situations:

* When an abstraction has one object dependent on the other. Encapsulation helps to separate the objects, thus allowing them to vary and be reused independently
* When changes in one object requires changes in others, all other dependent objects are required to update their states to maintain consistencies. 
* When the number of objects needed to be changed is not known, the object or subject does not need to know the number of observers it maintains.
* When there is a need to reduce coupling, when an object needs to notify other objects without knowing who these objects are.  

#### What are the benefits and liabilities of using this design pattern? ####

The benefits and liabilities of the Observer pattern are:

* Abstract coupling between Subject and Observer. The subject maintains a list of observers with each conforming to the simple interface of the abstract Observer class. The subject is not aware of the concrete class of the
observer. This creates loose and abstract coupling between subjects and observers.
* Loose coupling creates different levels of abstraction in the system. Lower level subjects can communicate with higher level observers, but keeping the ordering of the system intact. 
* Supports broadcast communications. Notifications are broadcast automatically to all objects that subscribed to it. The subject is loosely coupled with the observers that it communicates with so its only sole purpose is to notify its observers. This allows developers freedom to add and remove observers at any time. In addition, subject and observer objects can be reused separately. They can be modified independently. Thus, it can be used in cases where the number of state dependent objects are not known in advance. In addition, the number of objects can change over time as well since the the method allows for easy insertion and removal of observer objects.
* It avoids direct object interactions. It is beneficial in cases where one or more objects are interested in state changes of a given object.
* It creates loose coupling applications that helps to maintain the object state dependencies
* Communications can cause unexpected updates. Observers are independent and have no knowledge of one another. So, operations on the subject can cause a cascade of updates to the observers and its dependencies. When an observer changes the state of a subject, all of the dependent observers are notified. If dependency criteria is not well defined, this can cause recursive updates.
* For many cases, observers are only notified about state changes, but it is up to the observer to decide exactly what has changed and how to respond to it. This situation can be avoided by including additional information with the notifications, providing clues for the observer to know which states had changed.
* There can be inconsistencies in states so that state notifications are lost. The main issue is that the observer objects are independent and have no knowledge of one another. So, one observer object can change the state of the subject without notifying the other observers of the changes.
* It introduces indirection to maintain state consistency. Although this can increase flexibility in the design, it impede performance and decrease readability of the code.
* The subjects and observers are loosely coupled so there is no direct way for the observer to know about the deletion. So, deletion of the subject can cause the observers to have dangling references to the deleted subjects. 

#### Example Problem ####
An excellent example of the observer pattern is an online store that notifies shoppers when the products are available. When searching for a product there is an option to notify the customer when the product is available. Subscribing to this option causes states changes to the product once it is available to notify the observers to the customer by mail that the “Product is available to be purchased”.

#### UML Class Diagram ####

![alt text](https://gitlab.com/xnghiem/image/raw/master/store.jpg "Logo Title Text 1")

#### References for further information ####

https://www.java2blog.com/observer-design-pattern-in-java/
The reference above is a great tool for understanding the observer pattern. It breaks it down into its simplest components ob subjects, observers, and the concrete classes and proves easy to understand explanations in regards to the different components in order for the reader to thoroughly understand the structure of the pattern. 

https://dzone.com/articles/the-observer-pattern-using-modern-java
The reference above helps to explain the intent of the observer pattern. It provides uml diagrams for understanding the structure of the pattern and provides an example implementation of the zoo to illustrate the fundamentals of the observer pattern.
