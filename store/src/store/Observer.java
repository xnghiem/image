package store;

public interface Observer {
	  public void update(String availability);
}

